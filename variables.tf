variable "alb_arn" {
  description = "ARN for the ALB.  Use this if the ALB already exists."
  default = ""
}
variable "alb_port" {
  description = "port to use on the ALB"
  default = "80"
}
variable "app_count" {
  description = "The number of instances of the application that you want to run.  This doesn't currently do anything.  This may be used in the future as a limit on the number of instances, but currently isn't active in this module."
  default = 3
}
variable "app_download_url" {
  description = "The full path to the binary, zip, or package that you want to use in the cloud init"
  default = ""
}
variable "app_tpl_filepath_filename" {
  description = "Relative path to the template file that you want to use in the cloud init"
}
variable "app_version" {
  description = "The version of the application.  This is used in the cloud init"
  default = ""
}
variable "application_name" {
  description = "Name of the application"
}
variable "AWS_ACCESS_KEY"{ default = "" }
variable "aws_ami"{
  description = "The ami for the EC2 instances to be used in this module.  Note: this ami must be available in this region."
}
variable "AWS_SECRET_KEY"{ default = "" }
variable "bastion_host"{ default = "" }
variable "certificate_arn" {
  description = "If you use port 443 on the load balancer then you need a certificate. This is the ARN for the certificate that exists."
  default = ""
}
variable "ec2_size"{ default = "t2.small" }
variable "http_proxy" {
  description = "CIDR blocks that should use the proxy"
  default = ""
}
variable "key_name" { default = "" }
variable "NEXUS_PASSWORD" {
  description = "The Password to where the app_download_url requires a username/password"
  default = ""
}
variable "NEXUS_USERNAME" {
  description = "The Password to where the app_download_url requires a username/password"
  default = ""
}
variable "no_proxy" {
  description = "CIDR blocks that shouldn't use the proxy"
  default = "'localhost,http://127.0.0.1,10.0.0.0/8'"
}
variable "private_subnets" {
  description = "Private Subnet IDs of where the application is to be installed.  The correct format is brackets is a list of subnet IDs each in quotation marks and comma separated.  These are all enclosed in brackets."
}
variable "public_subnets" {
  description = "Public Subnet IDs of where the application is to be installed.  The correct format is brackets is a list of subnet IDs each in quotation marks and comma separated.  These are all enclosed in brackets."
}
variable "region"{ default = "us-gov-west-1" }
variable "ssh_cidrs" {
  description = "The CIDR that should be able to ssh to the newly created instances. This is typically only the bastion host or other machines in the private subnets."
}
variable "ssl_policy" { default = "" }
variable "vpc_id" {
  description = "VPC ID of where the application is to be installed"
}