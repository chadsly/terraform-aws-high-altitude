#output "region" {
#  value = var.region
#}

output "app_lb_dns" {
  value = aws_lb.app.dns_name
}

output "app_server_count" {
  value = length(aws_instance.app-servers)
}

output "app_ip_addrs" {
  value = [ 
    for instance in aws_instance.app-servers:
    instance.private_ip
  ]
}

output "app_lead_addrs" {
  value = element([
    for instance in aws_instance.app-servers:
    instance.private_ip
  ], 1)
}

output "app_dns" {
  value = [
    for instance in aws_instance.app-servers:
    instance.private_dns
  ]
}

output "private_app_dns" {
  value = [
    for instance in aws_instance.app-servers:                                                                                                                                                                                                    instance.private_dns                                                                                                                                                                                                                       ]
}

output "Deployment_Tag" {
  value = random_id.deployment_tag.hex
}

output "SSH_Key" {
  value = aws_key_pair.generated_key.key_name
}

output "pem_file" {
  value = tls_private_key.ssh.private_key_pem
}

output "app-servers-private-ip" {
  value = [
    for instance in aws_instance.app-servers:
    instance.private_ip
  ]
}
